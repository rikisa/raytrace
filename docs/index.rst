.. raytracer documentation master file, created by
   sphinx-quickstart on Tue Jan 25 16:49:34 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to raytracer's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Classes
=======
.. doxygenclass:: RTController
   :project: raytracer
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
