#pragma once

#include "core/linalg.hpp"

#include <limits>

constexpr double prec = std::numeric_limits<core::PosVec::elem_type>::epsilon();

#define is_close( a, b ) a == Approx( b ).margin( prec )
#define all_close_3d( a, b )                                                                       \
  ( ( a )[ 0 ] == Approx( ( b )[ 0 ] ).margin( prec ) &&                                           \
    ( a )[ 1 ] == Approx( ( b )[ 1 ] ).margin( prec ) &&                                           \
    ( a )[ 2 ] == Approx( ( b )[ 2 ] ).margin( prec ) )

// constexpr double eps = std::numeric_limits<PosVec::elem_type>::epsilon() * 1000;
// #define is_close(a, b) a == Approx(b).epsilon(eps)
// #define all_close_3d(a, b) (a[0] == Approx(b[0]).epsilon(eps) && a[1] ==
// Approx(b[1]).epsilon(eps) && a[2] == Approx(b[2]).epsilon(eps))
