#pragma once

#include "utils.hpp"

#include <array>

namespace basisvals {

constexpr std::array<double, 3> arr_one3 = { 1, 1, 1 };
constexpr std::array<double, 3> arr_zero3 = { 0, 0, 0 };
constexpr std::array<double, 3> arr_vert3 = { 1, 0, 0 };
constexpr std::array<double, 3> arr_hor3 = { 0, 1, 0 };
constexpr std::array<double, 3> arr_depth3 = { 0, 0, 1 };

} // namespace basisvals

namespace testvals {

constexpr std::array<double, 3> vec3_gt = { 1, 4, 2 };

constexpr double vec3_gt_l2 = 4.58257569495584000658804719372800848898445657676797190260724212\
3906868425547770886604361559493445032677600;

constexpr double vec3_gt_sum = 7;

constexpr std::array<double, 3> vec3_gt_normed = {
    0.2182178902359923812660974854156194518564026941318081,
    0.8728715609439695250643899416624778074256107765272327,
    0.4364357804719847625321949708312389037128053882636163 };

constexpr double vec3_gt_normed_sum = 1.527525231651946668862682397909336162994818858922657;

constexpr std::array<double, 3> vec3_lt = { 0.1, 0.2, 0.3 };

constexpr double vec3_lt_sum = 0.6;

constexpr double vec3_lt_l2 = 0.37416573867739413855837487323165493017560198077787269463037454\
6732003515630693902797680989519437957150099;

constexpr std::array<double, 3> vec3_lt_normed = {
    0.2672612419124243846845534808797535215540014148413376,
    0.5345224838248487693691069617595070431080028296826752,
    0.8017837257372731540536604426392605646620042445240129 };

constexpr double vec3_lt_normed_sum = 1.603567451474546308107320885278521129324008489048025;

constexpr double vec3_lt_dot_vec3_gt = 1.5;

constexpr double vec3_lt_cos_vec3_gt = 0.874817765279706463641887169537818354273552671663096;

} // namespace testvals
