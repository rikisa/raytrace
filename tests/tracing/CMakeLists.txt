add_executable( test_Tracing )
target_sources( test_Tracing
    PRIVATE
        test_main.hpp
        test_main.cpp
)
target_link_libraries( test_Tracing
    PUBLIC
        Catch2::Catch2
        core
        test_commons
)

catch_discover_tests( test_Tracing )
