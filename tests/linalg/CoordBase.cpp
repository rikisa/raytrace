#include "common/valueconstants.hpp"
#include "core/linalg.hpp"
#include "test_main.hpp"

using core::CoordBase;
using core::PosVec;

using basisvals::arr_depth3;
using basisvals::arr_hor3;
using basisvals::arr_vert3;
using basisvals::arr_zero3;

TEST_CASE( "CoordBase Default", "[CoordBase][core][linalg][constructors]" ) {

  auto zero = CoordBase<PosVec>();
  PosVec a_pos( { 1, 2, 3 } );

  REQUIRE( all_close_3d( zero.ydir, arr_vert3 ) );
  REQUIRE( all_close_3d( zero.xdir, arr_hor3 ) );
  REQUIRE( all_close_3d( zero.zdir, arr_depth3 ) );
  REQUIRE( all_close_3d( zero.pos, arr_zero3 ) );
  REQUIRE( cross( zero.ydir, zero.xdir ) == zero.zdir );

  // pos only
  auto moved = CoordBase<PosVec>( a_pos );
  REQUIRE( moved.pos == a_pos );

  // orientation only
  auto oriented = CoordBase<PosVec>( PosVec( arr_vert3 ), PosVec( arr_depth3 ) );
  REQUIRE( all_close_3d( oriented.ydir, arr_vert3 ) );
  REQUIRE( all_close_3d( oriented.xdir, arr_hor3 ) );
  REQUIRE( all_close_3d( oriented.zdir, arr_depth3 ) );
  REQUIRE( all_close_3d( oriented.pos, arr_zero3 ) );

  // flipped only
  auto flipped = CoordBase<PosVec>( -1.0 * PosVec( arr_vert3 ), PosVec( arr_depth3 ) );
  REQUIRE( flipped.ydir == -1.0 * PosVec( arr_vert3 ) );
  REQUIRE( flipped.xdir == -1.0 * PosVec( arr_hor3 ) );
  REQUIRE( all_close_3d( flipped.zdir, arr_depth3 ) );
  REQUIRE( all_close_3d( flipped.pos, arr_zero3 ) );

  // check normalization only
  auto rnd = CoordBase<PosVec>( PosVec( arr_vert3 ) * 2, a_pos );
  REQUIRE( rnd.ydir.norm() == 1 );
  REQUIRE( rnd.xdir.norm() == 1 );
  REQUIRE( rnd.zdir.norm() == 1 );
}
