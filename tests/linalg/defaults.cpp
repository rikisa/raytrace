#include "common/utils.hpp"
#include "core/linalg.hpp"
#include "test_main.hpp"

using core::ArrayN;
using core::ColorFormat;
using core::ColorVec;
using core::DArray3;
using core::DArray4;
using core::PosVec;

TEST_CASE( "ArrayN Constructors", "[ArrayN][core][linalg][constructors]" ) {

  // from list
  auto arr0 = ArrayN<double, 3>( { 1, 2, 3 } );
  // from other
  auto arr1 = ArrayN<double, 3>( arr0 );
  // default
  auto arr2 = ArrayN<double, 3>();
  // from const arr
  ArrayN<double, 3> arr3( { 2, 3, 4 } );
  // copy assignment
  arr2 = arr1;

  arr0 = { 2, 3, 4 };

  for ( auto i = 0; i < 2; ++i ) {
    REQUIRE( arr0[ i ] - 1 == arr1[ i ] );
    REQUIRE( arr1[ i ] == arr2[ i ] );
    REQUIRE( arr3[ i ] == arr0[ i ] );
  }

} // TEST_CASE

TEST_CASE( "Vector Defaults", "[Vector][core][linalg][defaults]" ) {

  // default values
  auto arr = ArrayN<double, 4>();

  REQUIRE( arr.cget()[ 0 ] == 0 );
  REQUIRE( arr.cget()[ 1 ] == 0 );
  REQUIRE( arr.cget()[ 2 ] == 0 );
  REQUIRE( arr.cget()[ 3 ] == 0 );

  PosVec pos;
  ColorVec color;

  REQUIRE( pos.dim == 3 );
  REQUIRE( color.format == ColorFormat::RGB );

} // TEST_CASE

TEST_CASE( "ArrayN Iterator", "[ArrayN][core][linalg][iterator]" ) {

  DArray3 test( { 7, 8, 9 } );

  // looping
  PosVec::elem_type ref = 7;
  for ( auto it : test ) {
    REQUIRE( it == ref );
    ref++;
  }

  // bidirectional, referecne operator
  auto it = test.begin();
  REQUIRE( *it == 7 );
  it += 2;
  REQUIRE( *it == 9 );
  it--;
  REQUIRE( *it == 8 );

  // writing
  for ( auto it = test.begin(); it != test.end(); ++it ) {
    *it = *it + 1;
    INFO( "*it = " << it );
  }
  REQUIRE( test.end() - test.begin() == 3 );

  // via []
  REQUIRE( is_close( test[ 0 ], 8 ) );
  REQUIRE( is_close( test[ 1 ], 9 ) );
  REQUIRE( is_close( test[ 2 ], 10 ) );

  // via iterator
  it = test.begin();
  REQUIRE( is_close( *it, 8 ) );
  it = test.end();
  REQUIRE( is_close( *( --it ), 10 ) );

} // TEST_CASE
