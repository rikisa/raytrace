#include "common/rayconstants.hpp"
#include "common/valueconstants.hpp"
#include "core/camera.hpp"
#include "core/image.hpp"
#include "core/linalg.hpp"
#include "core/typedefs.hpp"
#include "test_main.hpp"

#include <cmath>
#include <cstddef>

using core::Camera;
using core::ColorVec;
using core::CoordBase;
using core::Image;
using core::PosVec;

// NOLINTBEGIN(*-cognitive-complexity, *-magic-numbers)

// NOLINTNEXTLINE(*-cognitive-complexity, *-magic-numbers)
TEST_CASE( "Camera coords", "[Camera][core][rendering]" ) {

  constexpr size_t rows = 100;
  constexpr size_t cols = 100;
  constexpr double focal_len = 1.345;

  Image<ColorVec> img( rows, cols );
  CoordBase<PosVec> base;
  CoordBase<PosVec> base_nz( PosVec( { 1, 2, 3 } ) );

  Camera<ColorVec, PosVec> cam1( img );
  Camera<ColorVec, PosVec> cam2( img, 1, base );
  Camera<ColorVec, PosVec> cam_nz( img, 1, base_nz, focal_len );

  PosVec poi( { 1.0, 1.0, 1.0 } );
  PosVec vec_nz_poi( { 0.0, -1.0, -2.0 } ); // NOLINT(*-cognitive-complexity, *-magic-numbers)

  SECTION( "Defaults" ) {
    REQUIRE( cam1.orientation()->xdir == cam2.orientation()->xdir );
    REQUIRE( cam1.orientation()->ydir == cam2.orientation()->ydir );
    REQUIRE( cam1.orientation()->zdir == cam2.orientation()->zdir );
    REQUIRE( cam1.orientation()->pos == cam2.orientation()->pos );
    REQUIRE( cam_nz.orientation()->pos != cam2.orientation()->pos );
  }

  SECTION( "CoordBase is copied, changes in paramter are not reflected in Cam" ) {
    base_nz.pos = { 1, 1, 1 };
    REQUIRE( cam_nz.orientation()->pos == PosVec( { 1, 2, 3 } ) );
  }

  SECTION( "Referenced Image" ) {
    ColorVec new_col( { 0.5, 0.4, 1 } ); // NOLINT(*-magic-numbers)
    img.set_pixel( 5, 6, new_col );      // NOLINT(*-magic-numbers)
    REQUIRE( cam1.image().get_pixel( 5, 6 ) == new_col );
    REQUIRE( cam2.image().get_pixel( 5, 6 ) == new_col );
    REQUIRE( cam_nz.image().get_pixel( 5, 6 ) == new_col );
  }

  SECTION( "Point to something" ) {

    REQUIRE( cam_nz.orientation()->xdir == cam2.orientation()->xdir );
    REQUIRE( cam_nz.orientation()->ydir == cam2.orientation()->ydir );
    REQUIRE( cam_nz.orientation()->zdir == cam2.orientation()->zdir );
    REQUIRE( cam_nz.orientation()->pos == base_nz.pos );

    cam_nz.look_at( poi );

    REQUIRE( cam_nz.orientation()->zdir == vec_nz_poi / vec_nz_poi.norm() );
    REQUIRE( cam_nz.orientation()->pos == base_nz.pos );
  }

  SECTION( "Focus point" ) {
    REQUIRE( cam1.focus() == PosVec( { 0, 0, -1 } ) );
    REQUIRE( cam1.focus().norm() == 1 );
    REQUIRE( cam_nz.focus() == PosVec( { 1, 2, 3 - focal_len } ) );
    REQUIRE( ( cam_nz.focus() - base_nz.pos ).norm() == focal_len );

    cam_nz.look_at( poi );
    // does focal point turn?
    REQUIRE( cam_nz.focus() == base_nz.pos - ( cam_nz.orientation()->zdir * focal_len ) );
  }
}

// NOLINTNEXTLINE(*-cognitive-complexity, *-magic-numbers)
TEST_CASE( "Iterator interface", "[Camera][Ray][core][rendering]" ) {

  constexpr size_t rows = 21;
  constexpr size_t cols = 16;
  constexpr size_t img_width = 10;

  Image<ColorVec> img( rows, cols );
  Camera<ColorVec, PosVec> cam( img, img_width );
  auto ray_it = cam.begin();

  auto zero_vec = PosVec( 0 );

  SECTION( "Iterator end and begin" ) { REQUIRE( ray_it != cam.end() ); }

  SECTION( "Iterator increment changes base and origin" ) {
    PosVec const ori = PosVec( ray_it->origin );
    REQUIRE( all_close_3d( ori, ray_it->origin ) );
    ++ray_it;
    REQUIRE( ori != ray_it->origin );
    REQUIRE( ray_it->dir != zero_vec );
    REQUIRE( is_close( ray_it->dir.norm(), 1.0 ) );
  }

  SECTION( "Iterator inplace add changes base and origin" ) {
    PosVec const ori = PosVec( ray_it->origin );
    REQUIRE( all_close_3d( ori, ray_it->origin ) );
    ray_it += 1;
    REQUIRE( ori != ray_it->origin );
    REQUIRE( ray_it->dir != zero_vec );
    REQUIRE( is_close( ray_it->dir.norm(), 1.0 ) );
  }

  SECTION( "Iterator inc and inplace equivalen" ) {
    auto it_inp = cam.begin();
    auto it_inc = cam.begin();
    PosVec const ori_inpl = PosVec( it_inp->origin );
    PosVec const ori_inc = PosVec( it_inc->origin );
    REQUIRE( all_close_3d( ori_inpl, ori_inc ) );
    ++it_inc;
    ++it_inc;
    it_inp += 2;
    REQUIRE( it_inc->origin == it_inp->origin );
  }
}

// NOLINTNEXTLINE(*-cognitive-complexity, *-magic-numbers)
TEST_CASE( "Camera Iterator Range", "[Camera][Ray][Range][core][rendering]" ) {

  size_t cols = 3;
  size_t rows = 2;

  // startori from top left corner
  auto start_ori = PosVec( { 1.0 / 6.0, -1.0 / 3.0, 0 } ); // NOLINT(*-magic-numbers)
  auto col_step = PosVec( { 0, 1.0 / 3.0, 0 } );           // NOLINT(*-magic-numbers)

  Image<ColorVec> img( rows, cols );
  Camera<ColorVec, PosVec> cam( img, 1 );
  auto ray_it = cam.begin();
  auto const it_ori = PosVec( ray_it->origin );

  /*      -0.5                                    0.5
   *  0.33  +----------------+-------------+------+
   *        | (0.166, -0.33) |  (0.166, 0) |      |
   *        +----------------+-(0, 0)------+------+
   *        |                |             |      |
   *  0.33  +----------------+-------------+------+
   *
   */

  SECTION( "Iterator range starts correct" ) {
    for ( auto ray : cam ) {
      INFO(
          "Ought: " << start_ori << "\n"
                    << "   It: " << it_ori << "\n"
                    << "Range: " << ray.origin );
      // like the copy
      REQUIRE( all_close_3d( ray.origin, it_ori ) );
      // functionally ok
      REQUIRE( all_close_3d( ray.origin, start_ori ) );
      break;
    }
  }

  SECTION( "Multiple iterators share no state" ) {
    auto n = 2; // NOLINT(*identifier-length)
    auto ought = start_ori + col_step;
    auto last_ray_origin = PosVec( -1 );

    for ( auto ray : cam ) {
      if ( n-- == 0 ) {
        break;
      }
      last_ray_origin = ray.origin;
    }
    // other iterator is unchanged
    INFO(
        "Ought: " << ought << "\n"
                  << "   It: " << it_ori << "\n"
                  << "Range: " << last_ray_origin );
    REQUIRE( !all_close_3d( ought, ray_it->origin ) );
    // last ray from range is correct
    REQUIRE( all_close_3d( ought, last_ray_origin ) );
    ray_it += 1;
    INFO(
        "\n\nOught: " << ought << "\n"
                      << "   It: " << it_ori << "\n"
                      << "Range: " << last_ray_origin );
    REQUIRE( all_close_3d( ought, ray_it->origin ) );
  }

  SECTION( "Iterator range works and stops" ) {
    size_t i = 0; // NOLINT(*identifier-length)
    size_t last_col = 0;
    size_t last_row = 0;
    auto ought = PosVec(
        { -0.25 * ( 2.0 / 3.0 ), ( 1.0 / 3.0 ),
          0 } ); // NOLINT(*-cognitive-complexity, *-magic-numbers)
    auto last_ray_origin = PosVec( -1 );

    for ( auto ray : cam ) {
      REQUIRE( ray.origin == ray.base );
      last_row = i / img.cols;
      last_col = i % img.cols;
      REQUIRE( i++ <= img.size );
      REQUIRE( ray.origin != last_ray_origin );
      last_ray_origin = PosVec( ray.origin );
    }
    INFO( "(row, col) = (" << last_row << ", " << last_col << ")" );
    REQUIRE( i == img.size );
    REQUIRE( all_close_3d( last_ray_origin, ought ) );
    REQUIRE( last_col == cols - 1 );
    REQUIRE( last_row == rows - 1 );
  }
}

// NOLINTNEXTLINE(*-cognitive-complexity, *-magic-numbers)
TEST_CASE( "Camera Iterator Odd", "[Camera][Ray][core][rendering]" ) {

  constexpr size_t dim = 3; // odd
  constexpr double at_z = 3.0;

  Image<ColorVec> img( dim, dim );
  CoordBase<PosVec> base( PosVec( { 0, 0, at_z } ) );
  Camera<ColorVec, PosVec> cam( img, 1, base );
  auto ray_it = cam.begin();

  PosVec::elem_type step = ( 1.0 / static_cast<double>( dim ) );

  SECTION( "Ray Starting Offset" ) {
    // Ray origins in center of pixel in image plane...
    auto ought = PosVec( { step, -step, at_z } );
    INFO( ray_it->origin );
    INFO( ought );
    REQUIRE( all_close_3d( ray_it->origin, ought ) );
    REQUIRE( ray_it->origin == ray_it->base );
    REQUIRE( ray_it != cam.end() );
  }

  SECTION( "Bullseye Center is aligned with z dir of Coord base" ) {
    ray_it += 4;
    REQUIRE( ray_it->origin == base.pos );
    REQUIRE( ray_it->dir == base.zdir );
  }
}

// NOLINTNEXTLINE(*-cognitive-complexity, *-magic-numbers)
TEST_CASE( "Camera Iterator Even", "[Camera][Ray][core][rendering]" ) {

  constexpr size_t dim = 2;

  Image<ColorVec> img( dim, dim );
  Camera<ColorVec, PosVec> cam( img );
  auto ray_it = cam.begin();

  SECTION( "Ray Starting Offset" ) {
    // Ray origins in center of pixel in image plane...
    REQUIRE(
        ray_it->origin ==
        PosVec( { 0.25, -0.25, 0 } ) ); // NOLINT(*-cognitive-complexity, *-magic-numbers)
    REQUIRE( ray_it->origin == ray_it->base );
    REQUIRE( ray_it != cam.end() );
  }

  SECTION( "Ray Iteration" ) {
    // ...and iterate row wise: from top to bottom, from left to right in direction of z
    REQUIRE(
        ( ++ray_it )->origin ==
        PosVec( { 0.25, 0.25, 0 } ) ); // NOLINT(*-cognitive-complexity, *-magic-numbers)
    REQUIRE(
        ( ++ray_it )->origin ==
        PosVec( { -0.25, -0.25, 0 } ) ); // NOLINT(*-cognitive-complexity, *-magic-numbers)
    REQUIRE(
        ( ++ray_it )->origin ==
        PosVec( { -0.25, 0.25, 0 } ) ); // NOLINT(*-cognitive-complexity, *-magic-numbers)
  }

  SECTION( "Ray Iterator depleted (at end)" ) {
    ++ray_it; // -> 0, 1
    REQUIRE( ray_it != cam.end() );
    ++ray_it; // -> 1, 0
    REQUIRE( ray_it != cam.end() );
    ++ray_it; // -> 1, 1
    REQUIRE( ray_it != cam.end() );
    ++ray_it; // -> 2, 0 == end
    REQUIRE( ray_it == cam.end() );
  }
}

// NOLINTNEXTLINE(*-cognitive-complexity, *-magic-numbers)
TEST_CASE( "Camera Image Width", "[Camera][Image][core][rendering]" ) {
  // Testing if the image is scaled correctly

  using core::rt_float;

  constexpr rt_float rows = 5.0;
  constexpr rt_float cols = 6.0;
  constexpr rt_float image_width = 5.0;
  constexpr rt_float aspect = rows / cols;
  constexpr rt_float image_height = aspect * image_width;
  rt_float diag_len =
      // PosVec({image_height - aspect, image_width - aspect, 0}).norm();
      std::sqrt(
          ( ( image_height - aspect ) * ( image_height - aspect ) ) +
          ( ( image_width - aspect ) * ( image_width - aspect ) ) );

  /*        -2.083
   *     -2.5    -2.083    0          2.5
   * 2.08  +---+---+---+---+---+---+
   *       |   |   |   |   |   |   |
   *       +---+---+---+---+---+---+
   *       |   |   |   |   |   |   |
   *       +---+---+---+---+---+---+
   *    0  |   |   |   X   |   |   |
   *       +---+---+---+---+---+---+
   *       |   |   |   |   |   |   |
   *       +---+---+---+---+---+---+
   *       |   |   |   |   |   |   |
   * -2.08 +---+---+---+---+---+---+
   *
   */

  Image<ColorVec> img( rows, cols );
  Camera<ColorVec, PosVec> cam( img, image_width );

  auto ray_it = cam.begin();
  auto ray_ori = PosVec( ray_it->origin );
  auto img_center = cam.orientation()->pos;

  SECTION( "Distance between begin and img center" ) {
    auto v_dist = ( image_height - aspect ) * 0.5; // NOLINT
    REQUIRE( is_close( ray_it->origin[ 0 ] - img_center[ 0 ], v_dist ) );
    auto h_dist = -( image_width - aspect ) * 0.5; // NOLINT
    REQUIRE( is_close( ray_it->origin[ 1 ] - img_center[ 1 ], h_dist ) );
  }

  SECTION( "Distance between consecutive rays" ) {
    ray_it += 1;
    REQUIRE( is_close( ( ray_ori - ray_it->origin ).norm(), aspect ) );
  }

  SECTION( "Distance first col and last col in a row" ) {
    ray_it += ( cols - 1 );
    REQUIRE( is_close( ( ray_ori - ray_it->origin ).norm(), image_width - aspect ) );
  }

  SECTION( "Distance first row and last row along column" ) {
    ray_it += cols * ( rows - 1 );
    INFO( ray_ori );
    INFO( ray_it->origin );
    REQUIRE( is_close( ( ray_ori - ray_it->origin ).norm(), image_height - aspect ) );
    REQUIRE( is_close( ray_ori[ 1 ], ray_it->origin[ 1 ] ) );
  }

  SECTION( "Distance between begin and end is diagonal" ) {
    INFO( ray_it->origin );
    INFO( cam.end()->origin );
    ray_it += img.size - 1;
    REQUIRE( is_close( ( ray_ori - ray_it->origin ).norm(), diag_len ) );
  }
}

// TEST_CASE( "Camera applies image color", "[Camera][Image][core][rendering]" ) {
// }

// NOLINTNEXTLINE(*-cognitive-complexity, *-magic-numbers)
TEST_CASE( "Camera Rotated and Moved", "[Camera][Ray][core][rendering]" ) {

  using precalc_rays::rays;

  auto pos = PosVec( precalc_rays::pos );
  auto upward = PosVec( precalc_rays::base_y );
  auto forward = PosVec( precalc_rays::base_z );

  Image<ColorVec> img( precalc_rays::rows, precalc_rays::cols );

  CoordBase<PosVec> base( upward, forward, pos );
  Camera<ColorVec, PosVec> cam(
      img, precalc_rays::image_width, base, precalc_rays::focal_length ); // NOLINT

  auto ray_it = cam.begin();

  REQUIRE( all_close_3d( pos, base.pos ) );
  REQUIRE( all_close_3d( precalc_rays::base_x, base.xdir ) );
  REQUIRE( all_close_3d( precalc_rays::focus, cam.focus() ) );

  SECTION( "Ray have correct origin" ) {
    for ( auto i = 0; i < precalc_rays::ray_count; ++i ) {
      INFO(
          i << " Precalc > " << PosVec( rays.at( i ).origin ) << " != " << ray_it->origin
            << " < iterator" );
      REQUIRE( all_close_3d( ray_it->origin, rays.at( i ).origin ) );
      ++ray_it;
    }
  }

  SECTION( "Directions are normalized" ) {
    for ( auto i = 0; i < precalc_rays::ray_count; ++i ) {
      REQUIRE( is_close( ray_it->dir.norm(), 1.0 ) );
      ++ray_it;
    }
  }

  SECTION( "Ray have correct directions" ) {
    for ( auto i = 0; i < precalc_rays::ray_count; ++i ) {
      INFO( "Precalc > " << PosVec( rays.at( i ).dir ) << " != " << ray_it->dir << " < iterator" );
      REQUIRE( all_close_3d( ray_it->dir, rays.at( i ).dir ) );
      ++ray_it;
    }
  }
}

TEST_CASE( "Iterator applies color", "[Iterator][Image][core][rendering]" ) {

  constexpr size_t dim = 5;
  ColorVec a_color( { 0.5, 0.3, 0.6 } );    // NOLINT
  ColorVec b_color( { 0.1, 0.3, 0.4 } );    // NOLINT
  ColorVec mean_color( { 0.3, 0.3, 0.5 } ); // NOLINT
  core::ArrayN<core::rt_float, 3> zero = { { 0, 0, 0 } };

  Image<ColorVec> img( dim, dim );
  CoordBase<PosVec> base{};
  Camera<ColorVec, PosVec> cam( img );

  auto ray_it = cam.begin();
  ray_it->sample( a_color );
  ray_it.apply_current_ray();

  SECTION( "First Color" ) {
    INFO( "Ray Is: " << ray_it->color() );
    INFO( "Img Is: " << img.get_pixel( 0, 0 ) );
    INFO( "Ought: " << a_color );
    REQUIRE( all_close_3d( img.get_pixel( 0, 0 ), a_color ) );
  }

  SECTION( "Second Ray and Color" ) {
    REQUIRE( all_close_3d( img.get_pixel( 0, 0 ), a_color ) );
    ray_it++;
    ray_it->sample( b_color );
    REQUIRE( all_close_3d( img.get_pixel( 0, 1 ), zero ) );
    ray_it.apply_current_ray();
    REQUIRE( all_close_3d( img.get_pixel( 0, 1 ), mean_color ) );
  }
}

// NOLINTEND(*-cognitive-complexity, *-magic-numbers)
