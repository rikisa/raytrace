#include "core/image.hpp"

#include "common/utils.hpp"
#include "core/linalg.hpp"
#include "core/typedefs.hpp"
#include "core/util.hpp"
#include "test_main.hpp"

#include <iostream>

using core::ColorVec;
using core::Image;
using core::rt_index;

// NOLINTNEXTLINE(readability-*)
TEST_CASE( "Image", "[Image][core][rendering]" ) {

  constexpr size_t rows = 10;
  constexpr size_t cols = 6;

  constexpr rt_index row1 = 9;
  constexpr rt_index col1 = 3;

  constexpr rt_index rown = rows - 1;
  constexpr rt_index coln = cols - 1;

  Image<ColorVec> img( rows, cols );
  auto img_view = img.get_view();
  img.set_pixel( 0, 0, ColorVec( { 1, 1, 1 } ) );

  REQUIRE( img.get_channel( 0, 0, 0 ) == 1 );

  SECTION( "Rows/Cols/Array" ) {

    REQUIRE( img.cols == cols );
    REQUIRE( img.rows == rows );

    REQUIRE( img_view.cols == cols );
    REQUIRE( img_view.rows == rows );

    REQUIRE( img.get_pixel( row1, col1 ) == img_view.get_pixel( row1, col1 ) );
    REQUIRE( img.get_pixel( row1, col1 ) == ColorVec( { { 0, 0, 0 } } ) );
  }

  SECTION( "Ravel / Unravel" ) {
    auto idx = img.ravel_index( rown, coln );
    REQUIRE( idx == rows * cols * ColorVec::dim - 3 );
  }

  SECTION( "Manipulate data" ) {

    constexpr std::array<core::rt_float, 3> some_color{ 0.1, 0.34, 0.944 };
    constexpr core::rt_float test_val = 0.944;
    auto pixel = ColorVec( some_color );
    img.set_pixel( row1, col1, pixel );

    REQUIRE( img.get_pixel( row1, col1 ) == img_view.get_pixel( row1, col1 ) );
    REQUIRE( img.get_pixel( row1, col1 ) == pixel );

    REQUIRE( img.get_channel( row1, col1, 2 ) == test_val );
  }
}
