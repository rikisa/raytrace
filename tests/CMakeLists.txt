include(Catch)

add_subdirectory( common )

add_subdirectory( linalg )
# add_subdirectory( liblinalg )
add_subdirectory( engine )
# add_subdirectory( tracing )


if ( WITH_COVERAGE )
    message( STATUS "Building Tests with Coverage" )
    add_coverage_targets( test_Engine )
    add_coverage_targets( test_Linalg )
    # add_coverage_targets( test_Tracing )
endif()



