/* Everything rendering e.g. the Scene which contains objects and ray, which
 * interacts with the objects in a scene, or the camera which interacts with
 * the ray
 */
#pragma once

#include "core/camera.hpp"
#include "linalg.hpp"

#include <cstdint>
#include <iterator>
#include <memory>
#include <string>
#include <tuple>
#include <typeinfo>
#include <vector>

namespace core {

using std::vector;

template<class Color>
class Image;

/**
 * @brief Read-only view into imagedata
 *
 * Holds the image data, can only read it. Can only
 * constructed from the Image class
 *
 * @tparam Color Colorformat class (RGB, RGBA)
 */
template<class Color>
class ImageView {

  friend Image<Color>;

protected:
  struct rgba8_iterator;

public:
  using container_t = vector<typename Color::elem_type>;

  [[nodiscard]] auto get_channel( rt_index /*row*/, rt_index /*col*/, rt_index /*chan*/ ) const ->
      typename Color::elem_type;
  [[nodiscard]] auto get_pixel( rt_index /*row*/, rt_index /*col*/ ) const -> Color;

  [[nodiscard]] auto ravel_index( size_t /*row*/, size_t /*col*/ ) const noexcept -> size_t;
  [[nodiscard]] auto unravel_index( size_t /*index*/ ) const noexcept -> std::pair<size_t, size_t>;
  size_t rows;
  size_t cols;
  size_t size;

  void save( std::string const & /*filename*/ );

  [[nodiscard]] auto rgba8_begin() const -> rgba8_iterator;
  [[nodiscard]] auto rgba8_end() const -> rgba8_iterator;

  // ImageView( ImageView const & );
  // auto operator=( ImageView const & )-> ImageView<Color> & ;

  ImageView() = delete;

protected:
  ImageView( size_t /*rows*/, size_t /*cols*/, std::shared_ptr<container_t> data );
  void resize( size_t /*rows*/, size_t /*cols*/ );

private:
  std::shared_ptr<container_t> data;
};

/**
 * @brief Imagedata with access methods
 *
 * Extends the read-only access from ImageView to manipulation
 * of data
 *
 * @tparam Color Colorformat class (RGB, RGBA)
 */
// TODO(rikisa): refactor to private inheritance and invert linage
// ImageView ought to be child of Image, with dynamic RW Check
template<class Color = ColorVec>
class Image : public ImageView<Color> {

public:
  Image() = delete;
  Image( size_t /*rows*/, size_t /*cols*/ );

  // Image is only moveable, but image view can do whatever
  Image( Image const & ) = delete;
  auto operator=( Image ) -> Image & = delete;

  // moving
  Image( Image const && ) noexcept = delete;
  auto operator=( Image && ) noexcept -> Image & = delete;

  ~Image() = default;

  void load( std::string const & /*filename*/ );
  auto get_view() -> ImageView<Color>;
  void set_pixel( rt_index /*row*/, rt_index /*col*/, Color const & /*rgb*/ );
  void set_channel( rt_index, rt_index, rt_index );
};

template<class Color>
auto get_empty_view() -> ImageView<Color>;

template<class Color>
struct ImageView<Color>::rgba8_iterator {

  using iterator_category = std::input_iterator_tag;
  using value_type = std::uint8_t;
  using pointer = std::uint8_t *;
  using reference = std::uint8_t &;

  explicit rgba8_iterator( std::shared_ptr<container_t> /*data*/, size_t /*offset*/ = 0 );
  ~rgba8_iterator() = default;

  rgba8_iterator() = delete;
  rgba8_iterator( rgba8_iterator & ) = delete;
  auto operator=( rgba8_iterator const & ) -> rgba8_iterator & = delete;
  auto operator=( rgba8_iterator const && ) -> rgba8_iterator & = delete;

  auto operator*() -> std::uint8_t &;
  auto operator->() -> std::uint8_t *;
  auto operator++() -> rgba8_iterator &;
  auto operator++( int ) -> rgba8_iterator;

  void operator+=( size_t const & /*offset*/ );
  auto operator==( rgba8_iterator const & /*other*/ ) const -> bool;
  auto operator!=( rgba8_iterator const & /*other*/ ) const -> bool;

private:
  // for postfix increment, move out result of calculation
  rgba8_iterator( rgba8_iterator && ) noexcept = default;

  static std::uint8_t const MAX_VAL = 255;

  void increment_channel();

  void set_data();

  std::shared_ptr<container_t> m_data;
  std::size_t m_cur_pixel = 0;
  std::size_t m_cur_channel = 0;
  std::array<std::uint8_t, 4> m_cur_rgba8{};

  size_t m_at_channel = 0;
}; // iterator
} // namespace core
