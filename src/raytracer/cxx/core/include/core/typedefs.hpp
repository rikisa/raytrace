#pragma once

#include <array>
#include <cstdint>

namespace core {

using std::array; // NOLINT used in implementation files
using rt_size = std::size_t;
using rt_index = std::int64_t const;
using rt_float = double;

} // namespace core
