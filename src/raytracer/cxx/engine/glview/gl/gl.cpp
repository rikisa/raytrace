#include "gl.hpp"

#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

#include <iostream>
#include <stdexcept>
#include <utility>

namespace {
auto logger = spdlog::stdout_color_mt( "opengl" );
} // namespace

CanvasTexture::CanvasTexture( uint_t width, uint_t height, uint_t channels )
    : width( width ), height( height ), channels( channels ) {
  // data( width * height * channels ) {

  glGenTextures( 1, &this->tex_id );
  glActiveTexture( GL_TEXTURE0 ); // activating texture unit to bind to
  this->bind_texture();

  // operate on binded 2d texture, but not needed for now
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
};

void CanvasTexture::free_texture() {
  GLuint delete_me[] = { this->tex_id };
  glDeleteTextures( 1, delete_me );
  this->is_bound = false;
}

void CanvasTexture::bind_texture() {
  this->is_bound = true;
  glBindTexture( GL_TEXTURE_2D, this->tex_id );
}

CanvasTexture::~CanvasTexture() {
  if ( this->is_bound ) {
    this->free_texture();
  }
}

auto CanvasTexture::raw() -> void const * { return static_cast<void *>( this->data.data() ); }

/* Update and set new texture to use from this->data
 */
void CanvasTexture::update() {
  glTexImage2D(
      GL_TEXTURE_2D, 0, GL_RGB, this->width, this->height, 0, GL_RGB, GL_UNSIGNED_BYTE,
      this->raw() );
}; // CanvasTexture::update

template<typename T>
void CanvasTexture::from_data(
    uint_t cols, uint_t rows, uint_t channels, std::vector<T> const &data ) {
  this->width = cols;
  this->height = rows;
  this->channels = channels;
  this->data.resize( data.size() );

  for ( auto it = data.begin(); it != data.end(); ++it ) {
    this->data[ it - data.begin() ] = CanvasTexture::elem_type( *it * 255 );
  }

  // this->data = data; //data.begin(), data.end());
}

void CanvasObject::load() {

  // bind all stuff follwoing to Vertex array object
  glGenVertexArrays( 1, &this->vertarr_id );
  glBindVertexArray( this->vertarr_id );

  // create buffer object and bind to specific (unique) buffer type
  // here array buffere aka vertex buffer
  // binding -> reference buffer we operate on
  glGenBuffers( 1, &this->arrbuff_id );
  glBindBuffer( GL_ARRAY_BUFFER, this->arrbuff_id );
  glBufferData(
      GL_ARRAY_BUFFER, sizeof( this->object_data[ 0 ] ) * this->object_data.size(),
      this->object_data.data(),
      // GL_STATIC_DRAW // affects mem allocaition on GPU
      //                // static: changed once, read often; dynamic + changed often
      GL_DYNAMIC_DRAW // affects mem allocaition on GPU
  );

  // Element buffser for aggregating shared vertices into elements
  // Needs a bound array buffer
  glGenBuffers( 1, &this->elembuff_id );
  glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->elembuff_id );
  glBufferData(
      GL_ELEMENT_ARRAY_BUFFER, sizeof( this->indices[ 0 ] ) * this->indices.size(),
      this->indices.data(), GL_STATIC_DRAW );

  /* BUFFER LAYOUT
   * tell opengl how to interpret data in vertex buffer
   * will always refer to current vertex buffer
   * will be bound to current VAO
   */

  // vertices pos
  glVertexAttribPointer(
      0,        // index for this attribute
      3,        // elements per vertice
      GL_FLOAT, // type
      GL_FALSE, // normalize/clamp to -1,1 / 0,1
      /* number of bytes per vertex attribute
       * can be deduced from type and
       * size param, if tightly packed
       */
      8 * sizeof( this->object_data[ 0 ] ),
      (void *)nullptr // void pointer to first element
  );

  // vertices color
  glVertexAttribPointer(
      1,        // index for this attribute
      3,        // elements per vertice
      GL_FLOAT, // type
      GL_FALSE, // normalize/clamp to -1,1 / 0,1
      /* number of bytes per vertex
       * can be deduced from type and
       * size param, if tightly packed
       */
      8 * sizeof( object_data[ 0 ] ),
      (void *)( 3 * sizeof( object_data[ 0 ] ) ) // void pointer to first element
  );

  // vertices texture mapping
  glVertexAttribPointer(
      2,        // index for this attribute
      2,        // elements per vertice
      GL_FLOAT, // type
      GL_FALSE, // normalize/clamp to -1,1 / 0,1
      /* number of bytes per vertex
       * can be deduced from type and
       * size param, if tightly packed
       */
      8 * sizeof( object_data[ 0 ] ),
      (void *)( 6 * sizeof( object_data[ 0 ] ) ) // void pointer to first element
  );

  glEnableVertexAttribArray( 0 );
  glEnableVertexAttribArray( 1 );
  glEnableVertexAttribArray( 2 );

}; // CanvasObject::CanvasObject

const std::vector<float> CanvasObject::object_data = {
    // positions          // colors           // texture coords
    0.5F,  0.5F,  0.0F, 1.0F, 1.0F, 1.0F, 1.0F, 0.0F, // top right
    0.5F,  -0.5F, 0.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, // bottom right
    -0.5F, -0.5F, 0.0F, 1.0F, 1.0F, 1.0F, 0.0F, 1.0F, // bottom left
    -0.5F, 0.5F,  0.0F, 1.0F, 1.0F, 1.0F, 0.0F, 0.0F  // top left
};

const std::vector<uint_t> CanvasObject::indices = {
    0, 1, 3, // first triangle
    1, 2, 3  // second triangle
};

void RenderObserver::log_error( [[maybe_unused]] int error, char const *description ) {
  // std::cout << description;
  logger->error( "Error: {:s}", description );
}

// Callback for if the window is resized
void RenderObserver::fb_size_callback( GLFWwindow * /*unused*/, int width, int height ) {
  glClear( GL_COLOR_BUFFER_BIT );
  glViewport( 0, 0, width, height );
}

RenderObserver::RenderObserver( uint_t width, uint_t height, std::string const &title ) {
  glfwSetErrorCallback( this->log_error );

  if ( glfwInit() == 0 ) {
    // Initialization failed
    logger->error( "Could not initialize OpenGL" );
  } else {
    logger->info( "OpenGL initialized" );
  }

  // Opaque GLFWwindow can be wrapped if destructor call is passed as well
  // Handle for a window with some opengl context/state
  this->window = std::shared_ptr<GLFWwindow>(
      glfwCreateWindow( 640, 480, title.c_str(), nullptr, nullptr ), glfwDestroyWindow );

  if ( !this->window ) {
    logger->error( "Window and context creation failed" );
    throw std::runtime_error( "OpenGl Failed" );
  }
  logger->debug( "Created window and OpenGl context" );

  glfwMakeContextCurrent( this->window.get() );

  // let GLAD work on everything GLFW has build up so far
  auto procAddr = reinterpret_cast<GLADloadproc>( glfwGetProcAddress );
  if ( gladLoadGLLoader( procAddr ) == 0 ) {
    logger->error( "Could not load OpenGL entry points" );
  } else {
    // TODO(rikisa): /usr/include/fmt/core.h|1751 col 3| error: static_assert failed due to
    // requirement 'formattable_pointer' "Formatting of non-void pointers is disallowed."
    //  logger->debug(
    //      "OpenGL entry points loaded\n"
    //      "\t\tOpenGL Version: {:s}\n"
    //      "\t\tGLSL Version: {:s}\n",
    //      glGetString( GL_VERSION ), glGetString( GL_SHADING_LANGUAGE_VERSION ) );
  }

  // get frambuffer size and set viewport
  int dyn_width;
  int dyn_height;
  glfwGetFramebufferSize( this->window.get(), &dyn_width, &dyn_height );
  glViewport( 0, 0, dyn_width, dyn_height );
  if ( dyn_width != width or dyn_height != height ) {
    logger->warn(
        "Requested window size ({}, {}) but buffer is ({}, {})", width, height, dyn_width,
        dyn_height );
  }

  // set callbacks
  // glfwSetFramebufferSizeCallback( this->window.get(), this->fb_size_callback );
  // glfwSetKeyCallback( this->window.get(), this->key_callback );

  // vsync
  glfwSwapInterval( 1 );

  // set color for glClear (background color) clear buffer to background
  glClearColor( 0.2F, 0.3F, 0.3F, 1.0F );
  glClear( GL_COLOR_BUFFER_BIT ); // also: GL_DEPTH_BUFFER_BIT, GL_STENCIL_BUFFER_BIT

  // // pass variable pointers
  // // set user pointers
  // // glfwSetWindowUserPointer(this->get_window(), zoom_ptr );
  // // this->zoom = 1.0;
  // // glfwSetWindowUserPointer(this->get_window(), (void *) &this->zoom );

  this->canvas.load();
}

auto RenderObserver::get_window() -> GLFWwindow * { return this->window.get(); }

auto CanvasShaderProgram::make_shader( uint_t const &shader_id, std::string const &source )
    -> bool {
  char const *source_ptr = source.c_str();
  char const **line_ptr = &source_ptr;
  // expects a list of strings aka char**, stringlenght := NULL
  glShaderSource( shader_id, 1, line_ptr, nullptr );
  glCompileShader( shader_id );

  if ( this->check_compiled( shader_id ) ) {
    glAttachShader( this->prog_id, shader_id );
    return true;
  };
  return false;
}

auto CanvasShaderProgram::check_compiled( uint_t const &shader_id ) -> bool {
  // shader compiling error handling
  GLint success = 0;
  char infoLog[ 512 ];
  glGetShaderiv( shader_id, GL_COMPILE_STATUS, &success );
  if ( success == 0 ) {
    glGetShaderInfoLog( shader_id, 512, nullptr, infoLog );
    logger->debug( "SHADER COMPILE ERROR: {:s}", infoLog );
    return false;
  }

  logger->debug( "Shader({}) compiled successfully", shader_id );
  return true;
}

auto CanvasShaderProgram::check_linked() -> bool {
  // shader compiling error handling
  GLint success = 0;
  char infoLog[ 512 ];
  glGetProgramiv( this->prog_id, GL_LINK_STATUS, &success );
  if ( success == 0 ) {
    glGetProgramInfoLog( this->prog_id, 512, nullptr, infoLog );
    logger->debug( "SHADER LINK ERROR: {:s}", infoLog );
    return false;
  }
  logger->debug( "Shaders linked successfully", this->prog_id );
  return true;
}

void CanvasShaderProgram::use() const {
  if ( this->compiled and this->linked ) {
    glUseProgram( this->prog_id );
    logger->info( "Compiled shader successfully" );
  } else {
    logger->error( "Shader compilation failed" );
  }
};

auto CanvasShaderProgram::id() const -> int { return this->prog_id; };

void CanvasShaderProgram::make( std::string vert_source, std::string frag_source ) {

  this->prog_id = glCreateProgram();
  this->vertsh_id = glCreateShader( GL_VERTEX_SHADER );
  this->fragsh_id = glCreateShader( GL_FRAGMENT_SHADER );

  if ( this->make_shader( this->vertsh_id, std::move( vert_source ) ) and
       this->make_shader( this->fragsh_id, std::move( frag_source ) ) ) {
    this->compiled = true;

    glLinkProgram( this->prog_id );
    this->linked = this->check_linked();

    glDeleteShader( this->vertsh_id );
    glDeleteShader( this->fragsh_id );
  }
}

template void CanvasTexture::from_data<float>( uint_t, uint_t, uint_t, std::vector<float> const & );
template void
CanvasTexture::from_data<double>( uint_t, uint_t, uint_t, std::vector<double> const & );
