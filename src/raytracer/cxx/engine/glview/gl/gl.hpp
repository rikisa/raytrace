#pragma once

extern "C" {
// this order to let GLFW know that we manage extension ourselfes
// additionally explicit via definition
#define GLFW_INCLUDE_NONE

#include <GLFW/glfw3.h>
#include <glad/glad.h>
}

#include <memory>
#include <string>
#include <vector>

using uint_t = unsigned int;

class CanvasTexture {

public:
  CanvasTexture( uint_t /*width*/, uint_t /*height*/, uint_t /*channels*/ );
  CanvasTexture( CanvasTexture const & ) = delete;
  auto operator=( CanvasTexture const & ) -> CanvasTexture & = delete;
  ~CanvasTexture();

  void free_texture();

  using elem_type = uint8_t;

  void update();
  template<typename T>
  void from_data( uint_t, uint_t, uint_t, std::vector<T> const & );
  auto raw() -> void const *;
  std::vector<elem_type> data;

private:
  void bind_texture();
  bool is_bound = false;
  uint_t width;
  uint_t height;
  uint_t channels;
  uint_t tex_id{};
};

/*
 * CanvasObject storing all vertice data and opengl buffer data
 * Combines vertex array, buffer array element buffer and everything
 * else needed to render the canvas
 */
class CanvasObject {

public:
  CanvasObject() = default;
  CanvasObject( CanvasObject const & ) = delete;
  CanvasObject( CanvasObject && ) = default;
  auto operator=( CanvasObject const & ) -> CanvasObject & = delete;

  // reimplement via shader
  // void scale( float factor ) {
  //     for (int i = 0; i < 4; ++i) {
  //         for (int j = 0; j < 4; ++j) {
  //             this->object_data[i * 8 + j] *= factor;
  //         }
  //     }
  // }
  void update_verts();
  void load();

private:
  // triangle vertices
  static const std::vector<float> object_data;
  static const std::vector<unsigned int> indices;
  uint_t vertarr_id;
  uint_t arrbuff_id;
  uint_t elembuff_id;
};

/*
 * Very thin and basic abstraction of OpenGL ShaderPrograms
 * Hard coded shader code sufficient for the RenderObervers
 */
class CanvasShaderProgram {

public:
  CanvasShaderProgram() = default;
  CanvasShaderProgram( CanvasShaderProgram const & ) = delete;
  CanvasShaderProgram( CanvasShaderProgram && ) = default;
  auto operator=( CanvasShaderProgram const & ) -> CanvasShaderProgram & = delete;
  auto operator=( CanvasShaderProgram const && ) -> CanvasShaderProgram & = delete;
  void use() const;
  [[nodiscard]] auto id() const -> int;

  void make( std::string /*vert_source*/, std::string /*frag_source*/ );

private:
  std::string vertex_shader_src;
  std::string fragment_shader_src;

  uint_t prog_id{};
  uint_t vertsh_id{};
  uint_t fragsh_id{};

  bool compiled = false, linked = false;

  static auto check_compiled( uint_t const & /*shader_id*/ ) -> bool;
  auto check_linked() -> bool;
  auto make_shader( uint_t const &shader_id, std::string const &source ) -> bool;
};

/*
 * Very thin and basic abstraction of OpenGL context and window
 * Observes and displays a raw data buffer
 */
class RenderObserver {

public:
  RenderObserver( uint_t /*width*/, uint_t /*height*/, std::string const & /*title*/ );
  RenderObserver( RenderObserver const & ) = delete;
  auto operator=( RenderObserver const & ) -> RenderObserver & = delete;
  RenderObserver( RenderObserver && ) = default;
  auto operator=( RenderObserver const && ) -> RenderObserver & = delete;

  // float zoom;
  // void observe( CanvasData const & canvas );
  auto get_window() -> GLFWwindow *;

private:
  std::shared_ptr<GLFWwindow> window;
  // std::shared_ptr< CanvasData const > observee;
  static void log_error( int /*error*/, char const * /*description*/ );
  static void fb_size_callback( GLFWwindow * /*unused*/, int /*width*/, int /*height*/ );
  // static CanvasObject canvas;
  CanvasObject canvas{};
};
