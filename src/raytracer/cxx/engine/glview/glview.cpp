#include "glview.hpp"

#include "bindcore.hpp"
#include "core/linalg.hpp"
#include "gl.hpp"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

#include <algorithm>
#include <chrono>
#include <thread>
#include <utility>

using core::ColorVec;

namespace glview {

namespace {
// logger for this module
// static auto logger = spdlog::stdout_color_mt("glview");
// static auto file_logger = spdlog::basic_logger_mt("glviewf", "log.txt");
auto logger = spdlog::stdout_color_mt( "glview" );
auto current_view = get_active_image<ColorVec>();

} // namespace

GLViewer::GLViewer() { this->name = "GLViewer"; };

void GLViewer::start() {
  logger->debug( "Setting up GlViewer" );
  this->m_stopped = false;

  // auto obs = RenderObserver( 640, 480, "GlViewer" );
  // if ( obs.get_window() == nullptr ) {
  //     this->m_should_stop = true;
  // }

  // auto shader_prog = CanvasShaderProgram();
  // shader_prog.make( this->vert_src, this->frag_src );
  // this->vert_src.erase();
  // this->frag_src.erase();

  // auto content = CanvasTexture( current_view.rows, current_view.cols, 3 );

  while ( !this->m_should_stop ) {
    std::this_thread::sleep_for( std::chrono::milliseconds( 30 ) );
  }

  // int param_loc = glGetUniformLocation( shader_prog.id(), "param" );
  // shader_prog.use();
  // logger->debug( "Entering render loop" );
  // using std::chrono::operator""ms;
  // while ( !this->m_should_stop and !glfwWindowShouldClose( obs.get_window() ) ) {

  //     auto t_0 = std::chrono::steady_clock::now();

  //     // TODO Canditate for refactoring/prformance issues esp. update
  //     //  should use SubTexture2D (or what's its name?)
  //     content.from_data(
  //         current_view.cols, current_view.rows, ColorVec::dim, current_view.cget() );
  //     content.update();
  //     current_view = get_active_image<ColorVec>();
  //     // std::cout << img_view.rows << " x " << img_view.cols
  //     //           << "(0,0,0)=" << img_view.get_channel(0,0,0) << "\n";

  //     glClearColor( 0.2F, 0.3F, 0.3F, 1.0F );
  //     glClear( GL_COLOR_BUFFER_BIT );

  //     // placeholder for values from shader programm
  //     glUniform2f( param_loc, 1, 1 );

  //     glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr );

  //     glfwSwapBuffers( obs.get_window() );
  //     glfwPollEvents();

  //     // std::chrono::duration<double, std::milli> t_d =
  //     //         std::chrono::steady_clock::now() - t_0;
  //     std::this_thread::sleep_until( t_0 + 30ms );
  // }

  // // TODO
  // //  is be unnesescairy, per se as glfw cleans up.
  // //  however, otherwise we need to check for context or is still
  // //  aactive in CanvasTexture destructor
  // content.free_texture();
  // // // glfwTerminate is cranky, if there is context left...
  // // // We do not want cranky error messages at the end.
  // glfwSetErrorCallback( nullptr );
  // glfwTerminate();

  logger->debug( "Left render loop" );
  this->m_stopped = true;
  this->emit( event::Basic::STOP );
};

void GLViewer::stop() {
  if ( !this->m_stopped ) {
    logger->debug( "Recieved Stop" );
    this->m_should_stop = true;
  }
}

void GLViewer::set_sources( std::string vert_source, std::string frag_source ) {
  this->vert_src = std::move( vert_source );
  this->frag_src = std::move( frag_source );
  // logger->debug("Vertex Shader Source:\n\n{}\n\n", vert_source);
  // logger->debug("Fragment Shader Source:\n\n{}\n\n", frag_source);
}

// void GLViewer::observe( ImageView<ColorVec> image ) {
//     this->m_image = std::make_shared<ImageView<ColorVec>>( image );
// };

} // namespace glview
