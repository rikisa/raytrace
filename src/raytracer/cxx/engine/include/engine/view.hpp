#pragma once

#include "events.hpp"

#include <memory>
#include <thread>

namespace engine {

/** \brief Abstract baseclass the data viewer
 *
 * Takes a reference to an image view
 * makes no gurantees about what to do with the
 * image data, but not to change it. Should live in the
 * main thread to allow semless usage of eg. OpenGL
 * Is an observert, that can register to Control
 *
 */
class Viewer : public event::Observer {

public:
  // start funtion take over controll after is called
  void start() override = 0;
  void stop() override = 0;

  //    virtual void observe( ImageView<ColorVec>& image ) {
  //        this->m_image = std::make_shared<ImageView<ColorVec>>( image );
  //    };

  Viewer() = default;
  virtual ~Viewer() = default;
};

} // namespace engine
