#include "cputracer.hpp"

#include "core/linalg.hpp"

#include <fstream>
#include <iostream>

using core::Camera;
using core::ColorVec;
using core::PosVec;

template<class Color, class BaseVec>
void trace( Camera<Color, BaseVec> &cam, Scene<Color, BaseVec> const & /* scn */ ) {

  std::ofstream dmp( "raydump.csv" );
  dmp << "o_x;o_y;o_z;d_x;d_y;d_z\n";

  for ( auto ray : cam ) {
    // std::cout << "We want correct rays!\n";
    // std::cout << "(" << ray.origin[0] << ", " << ray.origin[1] << ")\n";
    // scn.intersect( ray );()
    dmp << ray.origin[ 0 ] << ";" << ray.origin[ 1 ] << ";" << ray.origin[ 2 ] << ";"
        << ray.dir[ 0 ] << ";" << ray.dir[ 1 ] << ";" << ray.dir[ 2 ] << "\n";
  }
}

template void trace( Camera<ColorVec, PosVec> &, Scene<ColorVec, PosVec> const & );
