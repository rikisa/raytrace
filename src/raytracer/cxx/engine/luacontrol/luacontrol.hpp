#pragma once

#include "bindcore.hpp"
#include "engine/control.hpp"
#include "sol/sol.hpp"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

#include <memory>
#include <string>

namespace luacontrol {

// move semantics and lua state
class LuaController : public engine::Controller {

public:
  virtual ~LuaController() = default;
  // only movable because of lua state. And only allow
  // to move the lua state to prevent bad things to happen...
  // ... it is not your segfault...
  explicit LuaController() {
    auto lua_state = sol::state();

    lua_state.open_libraries(
        sol::lib::base, sol::lib::math, sol::lib::string, sol::lib::io, sol::lib::os );

    this->m_lua_state = bind_engine_functions( std::move( lua_state ) );
  };
  explicit LuaController( sol::state lua_state ) : m_lua_state( std::move( lua_state ) ) {}

  LuaController( LuaController const & ) = delete;
  // explicit LuaController( engine::Controller&& other ) {};

  LuaController( LuaController &&other ) noexcept : m_lua_state( std::move( other.m_lua_state ) ) {}

  void run_script( std::string const &file_path ) {
    auto err_h = []( lua_State *, sol::protected_function_result pfr ) {
      sol::error err = pfr;
      // std::cout << "An error occurred: " << err.what() << std::endl;
      auto logger = spdlog::default_logger();
      logger->error( "Script had an error!\n\n{}\n", err.what() );
      return pfr;
    };
    auto lua = this->get_luastate();
    lua.safe_script_file( file_path, err_h );
  };

protected:
  [[nodiscard]] auto get_luastate() const -> sol::state_view {
    return { this->m_lua_state.lua_state() };
  };

private:
  sol::state m_lua_state;
};

} // namespace luacontrol
