/* Wrapps raw readline and lua code to do the interactive repl
 *
 */

#include "luascript.hpp"

#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

#include <utility>

namespace luacontrol {

// hidden for outside world
namespace {

// logger for this module
auto logger = spdlog::stdout_color_mt( "luacript" );

void _log_info( std::string msg ) { logger->info( msg ); }

} // namespace

Script::Script( std::string script_path ) : m_script_path( std::move( script_path ) ){};

Script::Script( sol::state sol_lua, std::string script_path )
    : LuaController( std::move( sol_lua ) ), m_script_path( std::move( script_path ) ){};

void Script::start() {
  logger->info( "Running Lua script at '{:s}'", this->m_script_path );
  auto lua = this->get_luastate();
  lua.set_function( "log", &_log_info );
  this->m_run_script();
  logger->info( "Finished Lua script at '{:s}'", this->m_script_path );
  this->emit( event::Basic::STOP );
};

void Script::stop(){};

void Script::set_script( std::string script_path ) {
  this->m_script_path = std::move( script_path );
};

void Script::m_run_script() { this->run_script( this->m_script_path ); }

} // namespace luacontrol
