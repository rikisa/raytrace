/* test embeding of lua interpreter into c++ using sol
 */

#pragma once

#include "luacontrol.hpp"

#include <memory>
#include <string>
#include <thread>

namespace luacontrol {

class Script : public LuaController {

public:
  virtual ~Script() = default;
  void start() override;
  void stop() override;
  void set_script( std::string /*script_path*/ );

  Script() = default;
  explicit Script( std::string /*script_path*/ );
  Script( sol::state /*sol_lua*/, std::string /*script_path*/ );
  Script( Script const & ) = delete;
  auto operator=( Script ) -> Script & = delete;

private:
  void m_run_script();
  std::string m_script_path;
};

} // namespace luacontrol
