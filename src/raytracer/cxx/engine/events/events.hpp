#pragma once
/*
 * Very basic EventHandling and notifier
 */

#include <memory>
#include <string>
#include <vector>

namespace event {

// basic controll, that start/stop the obersver
enum Basic : int { START, STOP };

class Observer;

class Handler {

public:
  Handler();
  Handler( Handler const & ) = delete;
  auto operator=( Handler ) -> Handler = delete;

  template<typename Event>
  void emit( Event /*event*/ );

  auto count_attached() -> int;

protected:
  void add( std::shared_ptr<Observer> /*observer*/ );
  std::string name = "Handler";

private:
  template<typename Event>
  void notify_observer( std::shared_ptr<Observer> /*observer*/, Event /*event*/, int & /*i*/ );
  std::vector<std::weak_ptr<Observer>> m_attached;

  friend void
      connect( std::shared_ptr<Observer> /*observer*/, std::shared_ptr<Handler> /*handler*/ );
};

class Observer {

  friend class Handler;

public:
  virtual ~Observer() = default;
  // generic start function, called on Event::START
  virtual void start(){};
  // generic stop function, called on Event::STOP
  virtual void stop(){};

  void listen( std::shared_ptr<Handler> /*handler*/ );

  void wait() const;

protected:
  // template<typename event>
  void emit( Basic event );
  void emit( int event );

  virtual void on_event( Basic /*event*/ );
  virtual void on_event( int /*unused*/ );

  auto observer_id() -> int;

  std::string name = "Observer";
  bool m_stopped = true;

private:
  std::weak_ptr<Handler> m_event_h;
};

/*
 * Connect Observer to Handler
 */
void connect( std::shared_ptr<Observer> /*observer*/, std::shared_ptr<Handler> /*handler*/ );

} // namespace event
