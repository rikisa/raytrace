#include "events.hpp"

#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

#include <chrono>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <thread>

namespace event {

namespace {
// logger for this module
auto logger = spdlog::stdout_color_mt( "event" );

// static std::map<Basic, std::string> const _event_names {
//     {Basic::STOP, "Stop"}, {Basic::START, "Start"}, };
//  premature optimization ;)
constexpr auto _event_names( int val ) {
  switch ( val ) {
  case Basic::STOP:
    return "Stop";
  case Basic::START:
    return "Start";
  default:
    return "Custom";
  }
}
} // namespace

Handler::Handler() { logger->debug( "Created new Eventhandler" ); }

void Handler::add( std::shared_ptr<Observer> observer ) {
  logger->debug( "Added {:s} for future notifications", observer->name );
  this->m_attached.emplace_back( observer );
}

auto Handler::count_attached() -> int { return this->m_attached.size(); }

template<typename Event>
void Handler::notify_observer( std::shared_ptr<Observer> observer, Event event, int &i ) {
  logger->debug( "{:d}/{:d}: {:s} ", i++, this->count_attached(), observer->name );
  observer->on_event( event );
}

template<typename Event>
void Handler::emit( Event event ) {
  logger->debug( "Emitting {:s} Event({:d})", _event_names( event ), int( event ) );

  int const i = 1;
  for ( auto observer : this->m_attached ) {
    if ( auto observer_spt = observer.lock() ) {
      this->notify_observer<Event>( observer_spt, event, i );
    } else {
      logger->warn( "Skipped attached observer!" );
    }
  }
}

// void Observer::emit( Basic const event ) {
//     if (this->m_event_h != nullptr) {
//         this->m_event_h->emit<>( event );
//     } else {
//         logger->warn("Can not emit {:s}({:d}): No Handler set/is nullptr)",
//                 _event_names(event), int(event));
//     }
// }
void Observer::emit( Basic const event ) {
  auto handler = this->m_event_h.lock();

  if ( !handler ) {
    logger->warn(
        "Can not emit {:s}({:d}): No Handler set/is nullptr)", _event_names( event ),
        static_cast<int>( event ) );
    return;
  }

  if ( handler != nullptr ) {
    handler->emit<>( event );
  } else {
    logger->warn(
        "Can not emit {:s}({:d}): No Handler set/is nullptr)", _event_names( event ),
        static_cast<int>( event ) );
  }
}

void Observer::listen( std::shared_ptr<Handler> handler ) { this->m_event_h = handler; }

// default interaction for Basic events
void Observer::on_event( Basic const event ) {
  switch ( event ) {
  case Basic::STOP:
    this->stop();
    break;
  case Basic::START:
    this->start();
    break;
  }
};

// no default actions on custom ( int events)
void Observer::on_event( int const /*unused*/ ){};

void connect( std::shared_ptr<Observer> observer, std::shared_ptr<Handler> handler ) {
  observer->listen( handler );
  handler->add( observer );
}

void Observer::wait() const {
  while ( !this->m_stopped ) {
    std::this_thread::sleep_for( std::chrono::duration<float, std::milli>( 5 ) );
  }
}

} // namespace event
