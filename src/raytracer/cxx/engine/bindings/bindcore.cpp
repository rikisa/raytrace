#include "bindcore.hpp"

#include "core/linalg.hpp"
#include "core/objects.hpp"
#include "cputracer.hpp"

#include <array>
#include <iostream>
#include <memory>
#include <string>

using core::Camera;
using core::ColorVec;
using core::CoordBase;
using core::Image;
using core::ImageView;
using core::PosVec;

namespace {

template<class Color>
auto current_view = core::get_empty_view<Color>();

template<class Color>
void dump_view() {
  std::cout << "Image [" << current_view<Color>.cols << "x" << current_view<Color>.rows << "]\n";
}

template<class Color>
void show( core::Image<Color> &img ) {
  current_view<Color> = img.get_view();
  std::cout << "Showing (" << img.cols << "x" << img.rows << ")\n";
}

template<class Color, class BaseVec>
Scene<Color, BaseVec> main_scene;

// template<class Color>
// void set_pixel( typename Color::array_type pixel ) {
//     auto as_vec = Color( pixel );
//     this->set_pixel( row, col, as_vec );
// }

template<class Color, class BaseVec, class Obj>
void _add_object( Scene<Color, BaseVec> *that, Obj obj ) {
  that->add_object( obj );
}

// template void show<ColorVec>(  std::shared_ptr<Image<ColorVec>> img );
template void show<core::ColorVec>( core::Image<core::ColorVec> &img );
} // namespace

template<class Color, class BaseVec>
void add_usertypes( sol::state &lua ) {

  auto main_scene = Scene<Color, BaseVec>();

  // Global Scene, only intersectable
  auto scene_t = lua.new_usertype<Scene<Color, BaseVec>>( "Scene", sol::no_constructor );
  scene_t[ "intersect" ] = &Scene<Color, BaseVec>::intersect;
  // scene_t[ "add_sphere" ] = &(Scene<Color, BaseVec>:: template add_object<Sphere>);
  scene_t[ "add_sphere" ] = _add_object<Color, BaseVec, Sphere<Color, BaseVec>>;

  auto coord_t = lua.new_usertype<CoordBase<BaseVec>>( "Coord", sol::no_constructor );

  auto image_t = lua.new_usertype<Image<Color>>(
      "Image", sol::constructors<Image<Color>( size_t, size_t )>() );
  // Use mor faimiliar width x height notation for lua
  // auto image_t = lua.new_usertype<Image<Color>>(
  //     "Image", sol::factories(
  //         [](size_t rows, size_t cols) { return std::make_shared<Image<Color>>( cols, rows ); }
  //         ));
  image_t[ "save" ] = &core::Image<Color>::save;
  // TODO(rikisa): _this
  //  image_t[ "set_pixel" ] = &set_pixel<Color>;

  auto camera_t = lua.new_usertype<Camera<Color, BaseVec>>(
      "Camera",
      sol::constructors<
          Camera<Color, CoordBase<BaseVec>>(
              Image<Color> &, typename BaseVec::elem_type, CoordBase<BaseVec>,
              typename BaseVec::elem_type ),
          Camera<Color, CoordBase<BaseVec>>(
              Image<Color> &, typename BaseVec::elem_type, CoordBase<BaseVec> ),
          Camera<Color, CoordBase<BaseVec>>( Image<Color> & )>() );

  auto material_t = lua.new_usertype<Material<Color>>(
      "Material", sol::constructors<Material<Color>( typename Color::array_type const )>() );

  auto sphere_t = lua.new_usertype<Sphere<Color, BaseVec>>(
      "Sphere",
      sol::constructors<
          Sphere<Color, BaseVec>( float, Material<Color>, typename BaseVec::array_type const ),
          Sphere<Color, BaseVec>( float, Material<Color> )>() );
  sphere_t[ "intersect" ] = &Sphere<Color, BaseVec>::intersect;
  sphere_t[ "set_material" ] = &Sphere<Color, BaseVec>::set_material;

  // set image view which should be shown
  lua[ "show" ] = show<Color>;

  lua[ "scn" ] = main_scene;

  // wont work bc lua is moved in, then moved out again
  // lua["run"] = [lua](std::string file_path){run(lua., file_path);};

  lua[ "dump_view" ] = dump_view<Color>;

  lua[ "trace" ] = trace<Color, BaseVec>;
}

[[nodiscard]] auto bind_engine_functions( sol::state lua ) -> sol::state {
  add_usertypes<ColorVec, PosVec>( lua );
  return lua;
}

template<class Color>
auto get_active_image() -> ImageView<Color> & {
  return current_view<Color>;
}

template auto get_active_image<ColorVec>() -> ImageView<ColorVec> &;
