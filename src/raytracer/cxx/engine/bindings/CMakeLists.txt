# Binding interface for core and tracer for usage in app
# either shared or static
if ( SHARED_LIBS )
    add_library( corebindings SHARED )
else()
    add_library( corebindings STATIC )
endif()

target_sources( corebindings
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/bindcore.hpp
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/bindcore.cpp
)

target_include_directories( corebindings PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} )

target_link_libraries( corebindings
    PUBLIC
        core
        cputracer
        sol2
)
