#version 410 core

out vec4 FragColor;
in vec3 ourColor;
in vec2 TexCoord;
uniform sampler2D ourTexture;
void main()
{
    // FragColor = vec4(ourColor, 1.0f);
    // FragColor = texture(ourTexture, TexCoord) * pow(TexCoord.x - TexCoord.y, 2);
    FragColor = texture(ourTexture, TexCoord);
    // FragColor = vec4(ourColor.x, ourColor.y, ourColor.z, 1.0f);
} 
