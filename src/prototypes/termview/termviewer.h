#include "core/image.hpp"
#include "core/linalg.hpp"

#include <array>
#include <cstddef>
#include <ncurses.h>

constexpr int KEY_Q = 113;
// constexpr std::array<char, 68> LUT
// {"$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/|()1{}[]?-_+~<>i!lI;:,^`'."};
constexpr std::array<char, 19> LUT{ ".'^,:;~+-?oa*#MB@$" };

class TermViewer {

  WINDOW *window;

public:
  TermViewer();
  ~TermViewer();

  void draw( core::ImageView<core::ColorVec> const & );
  void _update( core::ImageView<core::ColorVec> const &, size_t, size_t );
};
