// Simple test of readline

#include <iostream>

extern "C" {
#include "history.h"
#include "readline.h"
}

using namespace std;

int main( int argc, char **argv ) {
  char const *line;
  while ( ( line = readline( "? " ) ) != nullptr ) {
    cout << "[" << line << "]" << endl;
    if ( *line )
      add_history( line );
    line = NULL;
  }

  return 0;
}
