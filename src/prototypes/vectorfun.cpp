#include <array>
#include <iostream>

using namespace std;

template<typename T, int n>
class Base {
public:
  array<T, n> values = {};
  Base() = default;
  Base( array<float, 3> _values ) : values( _values ){};
};

template<typename T, int n>
class Derived : public Base<T, n> {
public:
  Derived() = default;
  Derived( array<float, 3> _values ) : Base<T, n>( _values ){};
};

typedef Derived<float, 3> D;

int main( int c, char **v ) {
  D a;
  array<float, 3> arr = { -1, 3, 69 };
  D b( arr );

  cout << b.values[ 0 ] << ", " << b.values[ 1 ] << ", " << b.values[ 2 ] << "\n";

  return 0;
}
