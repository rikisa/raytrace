/* test embeding of lua interpreter into c++ using sol
 */

#include "sol/sol.hpp"

#include <functional>
#include <iostream>
#include <string>
#include <tuple>
#include <vector>

class Dummy {
private:
  int a = 0;

public:
  void inc() { this->a++; };
  void dec() { this->a--; };
  void print() { std::cout << "now " << this->a << "\n"; };
};

class LuaParser {

public:
  LuaParser();
  void run_script( std::string );
  sol::state lua;

  template<typename Key, typename... Args>
  void set_function( Key &&key, Args &&...args ) {
    this->lua.set_function( key, args... );
  };
};

LuaParser::LuaParser() : lua() {
  // allowed libaries
  this->lua.open_libraries( sol::lib::base, sol::lib::math, sol::lib::string, sol::lib::io );
} // LuaParser::LuaParser

void LuaParser::run_script( std::string filename ) {
  auto eh = []( lua_State *state, sol::protected_function_result pfr ) {
    sol::error err = pfr;
    std::cout << "An error occurred: " << err.what() << std::endl;
    return pfr;
  };
  this->lua.safe_script_file( filename.c_str(), eh );
} // LuaParser::run_script

int main( int argc, char **argv ) {

  Dummy d;
  d.print();
  auto lp = LuaParser();
  lp.set_function( "inc_a", &Dummy::inc, &d );
  lp.set_function( "dec_a", &Dummy::dec, &d );
  lp.set_function( "print_a", &Dummy::print, &d );
  lp.run_script( "test.lua" );
  d.print();
}
