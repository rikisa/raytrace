#include <algorithm>
#include <array>
#include <bits/types/struct_timespec.h>
#include <chrono>
#include <iostream>
#include <ncpp/NotCurses.hh>
#include <ncpp/Plane.hh>
#include <ncpp/Visual.hh>
#include <notcurses/notcurses.h>
#include <ratio>
#include <string>
#include <thread>
#include <vector>
// #include <fmt/format.h>

auto main() -> int {

  notcurses_options opts{ .flags = NCOPTION_SUPPRESS_BANNERS | NCOPTION_PRESERVE_CURSOR };
  ncpp::NotCurses notc{ opts };

  // ncpp::Plane text{notc.get_stdplane()};
  auto *std_plane = notc.get_stdplane();
  // notc.mouse_enable(NCMICE_BUTTON_EVENT);

  unsigned rows = 0;
  unsigned cols = 0;
  std_plane->get_dim( rows, cols );

  // ncpp::Visual vis_plane{*std_plane, NCBLIT_PIXEL, 0, 0, rows, cols};

  ncplane_options txt_opts{
      .y = static_cast<int>( rows - 3 ), .x = 0, .rows = 3, .cols = cols, .flags = 0 };
  ncpp::Plane txt_plane{ std_plane, txt_opts };
  // notc.cursor_enable(static_cast<int>(rows - 3), txt_plane.get_abs_x());

  using ncpp::NCKey;

  std::vector<std::wstring> history{ 3 };
  history.reserve( 256 );
  wchar_t ret = 0;
  ncinput event{};
  unsigned atx = 0;
  unsigned aty = 0;
  std::wstring cur_line{};
  cur_line.reserve( 256 );
  int histat = history.size();

  while ( ret != L'q' ) {

    ret = notc.get( false, &event );

    switch ( ret ) {
    case NCKey::Enter:
      history.emplace_back( std::wstring( cur_line ) );
      histat = history.size();
      txt_plane.erase();
      cur_line.erase();
      break;
    case NCKey::Up:
      histat = std::max<int>( --histat, 0 );
      txt_plane.erase();
      cur_line = history.at( histat );
      break;
    case NCKey::Down:
      histat = std::min<int>( ++histat, history.size() - 1 );
      txt_plane.erase();
      cur_line = history.at( histat );
      break;
    case 0:
      break;
    case NCKey::Backspace:
      cur_line.pop_back();
      break;
    default:
      cur_line.push_back( ret );
    }

    txt_plane.printf( 0, 30, "histat: %d", histat );

    for ( int at_row = 0; at_row < 2; ++at_row ) {
      auto hist_line = history.at( history.size() - 2 + at_row );
      txt_plane.putstr( at_row, 0, hist_line.data() );
    }

    txt_plane.putstr( 2, 0, cur_line.data() );
    notc.render();

    std::this_thread::sleep_for( std::chrono::milliseconds( 30 ) );
  }

  return 0;
}
