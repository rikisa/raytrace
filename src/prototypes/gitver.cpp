#include "version.hpp"

#include <iostream>

int main( void ) {

  // Version mockvers = {
  //     .major=-1,
  //     .minor=-1,
  //     .revnum=-1,
  //     .commit="N/A",
  //     .all="-1.-1 (-1, N/A)"
  // };
  // std::cout << "Mock: " << mockvers.commit << "\n";

  std::cout << "GitVers: " << version::app.full << "\n";
  std::cout << u8"look mom, unicode! \u0394\n";
  return 0;
}
