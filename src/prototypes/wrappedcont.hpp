#include <iostream>
#include <memory>
#include <vector>

// dec
struct Base {
  int a = -5;
};

struct Deriv : Base {
  Deriv( int c );
  int b = 1;
  int c;
};

struct Container {
  std::vector<Base> objects;
  template<class O>
  void add( O obj );
};

struct Wrapper {
  Wrapper();
  std::shared_ptr<Container> container;
  template<class O, typename... ArgsT>
  void add( ArgsT... args );
};
