function(add_coverage_targets _target_name)
add_custom_target(newtarget  somecommand)
    if( COVERAGE )
        add_custom_target( ${_target_name}-ccov-preprocessing
            COMMAND
                ${CMAKE_COMMAND} -E env LLVM_PROFILE_FILE=${_target_name}.profraw $<TARGET_FILE:${_target_name}> || exit 0
            COMMAND
                llvm-profdata merge -sparse ${_target_name}.profraw -o ${_target_name}.profdata
            DEPENDS
                ${_target_name}
        )

        add_custom_target( ${_target_name}-ccov-show
            COMMAND
                llvm-cov show $<TARGET_FILE:${_target_name}> -instr-profile=${_target_name}.profdata -show-line-counts-or-regions                       
            DEPENDS                                                    
                ${_target_name}-ccov-preprocessing                     
        )

        add_custom_target( ${_target_name}-ccov-report                 
            COMMAND                                                    
                llvm-cov report $<TARGET_FILE:${_target_name}> -instr-profile=${_target_name}.profdata                
            DEPENDS                                                    
                ${_target_name}-ccov-preprocessing                     
        )

        add_custom_target( ${_target_name}-ccov                        
            COMMAND                                                    
            llvm-cov show $<TARGET_FILE:${_target_name}> -instr-profile=${_target_name}.profdata -show-line-counts-or-regions -output-dir=${CMAKE_BINARY_DIR}/coverage/${_target_name}-llvm-cov -format="html"
            DEPENDS
                ${_target_name}-ccov-preprocessing
        )

    endif()
endfunction()

if( COVERAGE )
    add_compile_options(
        -fprofile-instr-generate
        -fcoverage-mapping
    )
    add_link_options(
        -fprofile-instr-generate
        -fcoverage-mapping
    )
endif()
