ExternalProject_Add(
    pybind11
    GIT_REPOSITORY https://github.com/pybind/pybind11.git
    GIT_TAG "origin/stable"
    PREFIX "${ThirdPartyInstall}"
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    INSTALL_COMMAND ""
    UPDATE_COMMAND ""
    # CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${ThirdPartyInstall}
)

ExternalProject_Get_property(pybind11 SOURCE_DIR)

set( PYBIND_HEADERS "${SOURCE_DIR}/include/" )
list( APPEND ALL_INCLUDES ${PYBIND_HEADERS} )
