find_program( CLANG_TIDY_CMD
  run-clang-tidy 
  DOC
    "Clang Tidy Script")

if( (${CLANG_TIDY_CMD} STREQUAL "CLANG_TIDY_CMD-NOTFOUND"))
    MESSAGE( STATUS "Did Not found '${CLANG_TIDY_CMD}'")
    return()
endif()

add_custom_target(run_clang_tidy
    ${CLANG_TIDY_CMD} -j 10 -fix -format -style file
)

add_custom_target(run_clang_format
    ${CLANG_TIDY_CMD} -j 10 -format -style file -p ${CMAKE_BINARY_DIR}
)
