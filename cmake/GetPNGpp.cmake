ExternalProject_Add(
    png++
    URL http://download.savannah.nongnu.org/releases/pngpp/png++-0.2.9.tar.gz
    URL_HASH SHA1=024ce64b0a473984637ab2be1ef535cc744488e9
    PREFIX "${EXTERNAL_PROJECT_DIR}"
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    INSTALL_COMMAND ""
    UPDATE_COMMAND ""
    # CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${ThirdPartyInstall}
)

ExternalProject_Get_property(png++ SOURCE_DIR)

set( PNGPP_HEADERS ${SOURCE_DIR} )
list( APPEND ALL_INCLUDES ${PNGPP_HEADERS} )
