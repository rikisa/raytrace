FetchContent_Declare(
  sol2_repository
  GIT_REPOSITORY https://github.com/ThePhD/sol2.git
  GIT_TAG "v3.3.0"
  GIT_SHALLOW ON
  GIT_PROGRESS ON)

if(NOT sol2_repository_POPULATED)
  FetchContent_Populate(sol2_repository)
  add_subdirectory(${sol2_repository_SOURCE_DIR} ${sol2_repository_BINARY_DIR}
                   EXCLUDE_FROM_ALL)
endif()

# Implies -> FetchContent_GetProperties( sol2_repository  SOURCE_DIR
# SOL2_SOURCE_DIR ) add_subdirectory( ${SOL2_SOURCE_DIR} )
# FetchContent_MakeAvailable( sol2_repository )

# idk why anymore
get_target_property(SOL_INCLUDE_DIRS sol2 INTERFACE_INCLUDE_DIRECTORIES)
set_target_properties(sol2 PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "")
target_include_directories(sol2 SYSTEM INTERFACE ${SOL_INCLUDE_DIRS})
